﻿1.  Clone Setup repository (to ~/Setup)

1.  Install packages

2.  Install gvim configuration

    1. Clone repositiory (to ~/Vim)
    2. Substitute .vimrc (mv bin/.vimrc_Linux ~/.vimrc)
    3. run install (./install/build.py)
    4. install fonts

        mv Stuff/Fonts/* ~/.local/share/fonts/
        fc-cache -vf

3. Install linux keyboard hook

   1. Clone reporitory (to ~/LinuxKeyboardHook)

   2. For Ubuntu,

       cd ~/LinuxKeyboardHook/Writer/
       insmod LinuxKeyboardHookWriter.ko
       lsmod
       cd ~/LinuxKeyboardHook/Reader/
       sudo mv ./build/LinuxKeyboardHookReader /usr/bin

4.  Install conky configuration

5. .gitconfig user name

6. chsh -s /bin/zsh slava
