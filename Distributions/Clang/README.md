Clang Binaries
=

    +----------------+--------------------------+
    | Platforms:     | Win64                    |
    | Components:    | llvm; clang; clang-extra |
    | Clang Version: | 3.3                      |
    +----------------+--------------------------+
