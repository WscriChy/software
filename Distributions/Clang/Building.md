## Preparations

1. Checkout LLVM:

	Change directory to where you want the llvm directory placed.

		svn co http://llvm.org/svn/llvm-project/llvm/trunk src

2. Checkout Clang:

		cd src/tools
		svn co http://llvm.org/svn/llvm-project/cfe/trunk clang
		cd ../..

3. Checkout extra Clang Tools: (optional)

		cd src/tools/clang/tools
		svn co http://llvm.org/svn/llvm-project/clang-tools-extra/trunk extra
		cd ../../../..

4. Checkout Compiler-RT (optional, not for Windows):

		cd src/projects
		svn co http://llvm.org/svn/llvm-project/compiler-rt/trunk compiler-rt
		cd ../..

### One Shot

	svn co http://llvm.org/svn/llvm-project/llvm/trunk src && cd src/tools && svn co http://llvm.org/svn/llvm-project/cfe/trunk clang && cd clang/tools && svn co http://llvm.org/svn/llvm-project/clang-tools-extra/trunk extra && cd ../../../../

## Building

### Warnings

+ Don't forget to remove other clang distributions from path.

+ `CMAKE_CXX_FLAGS`

	CMake Warning:
		Manually-specified variables were not used by the project:
	
		CMAKE_CXX__FLAGS

**Solution:**   
Don't build use this flag.

+ `compiler-rt`

	d:/LLVM/build/src/projects/compiler-rt/lib/enable_execute_stack.c:13:22: fatal error: sys/mman.h: No such file or directory

**Solution:**   
Don't build `compiler-rt`, because it is an optional step.

### Commands

	mkdir install
	mkdir build
	cd build
	cmake.exe -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="../install" -DLLVM_TARGETS_TO_BUILD=X86 -DCMAKE_C_FLAGS="-O3" -DCMAKE_EXE_LINKER_FLAGS="-static-libstdc++ -static-libgcc -static" -DCMAKE_SHARED_LINKER_FLAGS="-static-libstdc++ -static-libgcc -static" -G "MSYS Makefiles" ../src
	make
	make install

### One Shot

	mkdir install && mkdir build && cd build && cmake.exe -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="../install" -DLLVM_TARGETS_TO_BUILD=X86 -DCMAKE_C_FLAGS="-O3" -DCMAKE_EXE_LINKER_FLAGS="-static-libstdc++ -static-libgcc -static" -DCMAKE_SHARED_LINKER_FLAGS="-static-libstdc++ -static-libgcc -static" -G "MSYS Makefiles" ../src && make && make install
