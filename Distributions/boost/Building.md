Boost.Build is a text-based system for developing, testing, and installing software. First, you'll need to build and install it. To do this:

Go to the directory tools\build\v2\.
Run bootstrap.bat
Run b2 install --prefix=PREFIX where PREFIX is the directory where you want Boost.Build to be installed
Add PREFIX\bin to your PATH environment variable.

b2 -j 8 --build-type=complete variant=debug,release link=shared,static threading=single,multi address-model=64 toolset=gcc cxxflags="-O3" cflags="-O3" linkflags="-static-libstdc++ -static-libgcc -static" runtime-link=shared,static --prefix=D:/boost/1.54.0 install

b2 -j 8 --build-type=complete variant=debug,release link=shared,static threading=single,multi address-model=64 toolset=gcc cxxflags="-O3" cflags="-O3" linkflags="-static-libstdc++ -static-libgcc -static" runtime-link=shared,static --prefix=D:/boost/1.54.0 install

Light-weight Linux installation
=

1.
cd boost

2.
./bootstrap.sh --prefix=/work/tatue2013/cfd-lab/lbm_gpu/lib/bjam

3.
./b2 install --prefix=/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0


ssh -X mikerov@atsccs68.informatik.tu-muenchen.de

export PATH=/home/software/gcc/gcc-4.7.0/bin/:$PATH
export PATH=/home/software/gcc/gcc-4.7.0/lib_manually_imported/:$PATH

export LD_LIBRARY_PATH=/home/software/gcc/gcc-4.7.0/lib/:$LD_LIBRARY_PATH
export LIBRARY_PATH=/home/software/gcc/gcc-4.7.0/lib_manually_imported
export LIBRARY_PATH=/usr/lib/x86_64-linux-gnu

cmake -DCMAKE_CXX_COMPILER=/home/software/gcc/gcc-4.7.0/bin/g++ -DCMAKE_C_COMPILER=/home/software/gcc/gcc-4.7.0/bin/gcc -DCMAKE_STATIC_LINKING=ON -G "Unix Makefiles" ../

