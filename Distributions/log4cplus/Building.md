log4cplus
=

## Building

### Warnings

+ Was not able to compite using `cmake`

### Commands

+ Without arguments

	mkdir install
	mkdir build
	cd build
	./../src/configure --prefix=$(realpath ../install)
	make
	make install

+ `-O3 -std=c++11`

	mkdir install
	mkdir build
	cd build
	./../src/configure CFLAGS="-O3 -std=c++11" CXXFLAGS="-O3 -std=c++11" --prefix=$(realpath ../install)
	make
	make install

### One Shot

+ Without arguments

	mkdir install && mkdir build && cd build && ./../src/configure --prefix=$(realpath ../install) && make && make install

+ `-O3 -std=c++11`

	mkdir install && mkdir build && cd build && ./../src/configure CFLAGS="-O3 -std=c++11" CXXFLAGS="-O3 -std=c++11" --prefix=$(realpath ../install) && make && make install
