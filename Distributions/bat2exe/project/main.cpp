#include <windows.h>

#include <stdio.h>
#include <string>

int
main(int argc, char* argv[]) {
  std::string command   = "D:\\msys2\\bin\\bash.exe";
  std::string arguments = command;

  for (int i = 1; i < argc; ++i) {
    if (!arguments.empty()) {
      arguments += " ";
    }

    arguments += argv[i];
  }

  printf("%s", command.c_str());
  printf(" %s", arguments.c_str());

  STARTUPINFO nowStartupInfo;
  GetStartupInfo(&nowStartupInfo);

  STARTUPINFO startupInfo;
  size_t      startupInfoStructureSize = sizeof(startupInfo);
  ZeroMemory(&startupInfo, startupInfoStructureSize);
  startupInfo.cb         = startupInfoStructureSize;
  startupInfo.dwFlags    = STARTF_USESTDHANDLES;
  startupInfo.hStdOutput = nowStartupInfo.hStdOutput;
  startupInfo.hStdError  = nowStartupInfo.hStdError;
  startupInfo.hStdInput  = nowStartupInfo.hStdInput;

  PROCESS_INFORMATION processInformation;
  ZeroMemory(&processInformation, sizeof(PROCESS_INFORMATION));

  if (!CreateProcess(const_cast<LPCSTR>(command.c_str()),
                     const_cast<LPSTR>(arguments.c_str()), 0, 0, TRUE,
                     0, 0, 0,
                     &nowStartupInfo, &processInformation)) {
    printf("Executable forwarder: Error(%i) Occured with %s %s",
           GetLastError(), command.c_str(), arguments.c_str());
  }

  return 0;
}
