uncrustify
=

## Building

### Warnings

+ Was not able to compite using `cmake`

### Commands

	mkdir install
	mkdir build
	cd build
	./../src/configure CFLAGS="-O3 -std=c++11 -flto" CXXFLAGS="-O3 -std=c++11 -flto" LDFLAGS="-static-libstdc++ -static-libgcc -static -flto" --prefix=$(realpath ../install)
	make
	make install

### One Shot

	mkdir install && mkdir build && cd build && ./../src/configure CFLAGS="-O3 -std=c++11 -flto" CXXFLAGS="-O3 -std=c++11 -flto" LDFLAGS="-static-libstdc++ -static-libgcc -static -flto" --prefix=$(realpath ../install) && make && make install
