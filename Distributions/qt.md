

Linux


./configure -release -opensource -confirm-license -fast -no-webkit -nomake examples -nomake demos -nomake docs -prefix "/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5"

# make sub-src


./configure.py --prefix "/work/tatue2013/cfd-lab/lbm_gpu/lib/oglplus" --include-dir "/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/include" --library-dir "/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/lib" --include-dir "/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/include" --library-dir "/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/lib" --no-docs --no-examples

export PATH=/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/bin:$PATH

export LD_LIBRARY_PATH=/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/lib:/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/lib:/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/bin:/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/lib:$LD_LIBRARY_PATH

export C_INCLUDE_PATH=/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/include:/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/include:/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/include

export C_INCLUDE_PATH=/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/include:/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/include:/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/include

cmake -DCMAKE_INSTALL_PREFIX="/work/tatue2013/cfd-lab/lbm_gpu/lib/eigen/3.2.0" -G "Unix Makefiles" ../eigen/

cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="/work/tatue2013/cfd-lab/lbm_gpu/lib/oglplus" -DLIBRARY_SEARCH_PATHS="/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/lib;/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/lib;/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/bin;/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/lib" -DCMAKE_CXX_FLAGS="-I/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/include -I/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/include -I/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/include" -DOGLPLUS_NO_EXAMPLES=ON -DOGLPLUS_NO_DOCS=ON .

cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="/work/tatue2013/cfd-lab/lbm_gpu/lib/oglplus" -DLIBRARY_SEARCH_PATHS="/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/lib;/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/lib;/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/bin;/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/lib" -DHEADER_SEARCH_PATHS="/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/include;/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/include;/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/include" -DOGLPLUS_NO_EXAMPLES=ON -DOGLPLUS_NO_DOCS=ON .

alias gcc="gcc -I/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/include -I/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/include -I/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/include -L/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/lib -L/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/lib -L/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/bin -L/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/lib"

alias g++="g++ -I/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/include -I/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/include -I/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/include -L/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/lib -L/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/lib -L/work/tatue2013/cfd-lab/lbm_gpu/lib/qt/4.8.5/bin -L/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/lib"

export BOOST_ROOT=/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0
export Boost_INCLUDE_DIR=/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/include
export Boost_LIBRARY_DIR=/work/tatue2013/cfd-lab/lbm_gpu/lib/boost/1.54.0/lib
export GLEW_INCLUDE_DIR=/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/include
export GLEW_LIBRARY_DIR=/work/tatue2013/cfd-lab/lbm_gpu/lib/glew/1.10.0/lib
export Eigen3_INCLUDE_DIR=/work/tatue2013/cfd-lab/lbm_gpu/lib/eigen/3.2.0/include
export OGLplus_INCLUDE_DIR=/work/tatue2013/cfd-lab/lbm_gpu/lib/oglplus
export OpenCL_INCLUDE_DIR=/work/tatue2013/cfd-lab/lbm_gpu/lib/opencl
export OpenCL_LIBRARY_DIR=/usr/lib64
