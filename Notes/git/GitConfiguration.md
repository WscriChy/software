Git Configuration
=

	git config --global user.email "WscriChy@gmail.com"
	git config --global user.name "WscriChy"

	git config user.email "SlavaMikerov@gmail.com"
	git config user.name "ViacheslavMikerov"

	git config user.email "CanisMajorWuff@gmail.com"
	git config user.name "CanisMajor"

	git config core.autocrlf false

	git config core.ignorecase false

Git Commands
=

## Git Branches

+ To see all branches

	git branch -a

+ Checkout remote branch

	git checkout -b develop origin/develop
