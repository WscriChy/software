To fix gtk theme in qt applications


* sudo apt-get install qtcurve

* sudo apt-get install qt4-qtconfig

* /usr/lib/x86_64-linux-gnu/qt4/bin/qtconfig

* Set gtk+ or qtcurve

* ? sudo ln -s /usr/lib/kde4/libexec/kdesu-distrib/kdesu /usr/bin/kdesu
 
The KDE is saving the own settings to the /home/[user]/.kde/...

The color settings are saved to the /1/:

    Colors

    The colors file follows the standard KDE colorscheme file format and allows a theme to define what colors work best with its theme elements...

    Saved at /home/[user]/.kde/share/apps/color-schemes/[unique name].colors ...

If you are editing the "current" color scheme then the settings are saved to the kdeglobals file - /home/[user]/.kde/share/config/kdeglobals.



================================================================================

Log off KDE and:

$ cd /var/tmp/kdecache-$USER
$ rm icon-cache.kcache
