
1. Main Installer

2. Go to [Binaries](http://www.lfd.uci.edu/~gohlke/pythonlibs/)

3. Cython

4. NumPy

5. Setuptools

		ez_setup.py

6. Pip

	Download and

		python setup.py install

7. Distibute

	`Distibute` is a subsitution for `Setuptools`.
	Now the installation could be performed:

		easy_install PackageName

8. `Swig` build system

9. `Enable`

	Download and

		python setup.py install

10. `Jedi`

		pip install jedi

11. `pyflake`

easy_install configobj 
easy_install nose    

    export ETS_TOOLKIT=qt4
    export QT_API=pyqt

export ETS_TOOLKIT=qt4 + export QT_API=pyside


C:\Python27\Lib\distutils\distutils.cfg to fix the vcvarsall.bat error.

[build]
compiler=mingw32

pexports.exe python27.dll > python27.def
dlltool --dllname python27.dll --def python27.def --output-lib libpython27.a

pexports.exe msvcr90.dll > msvcr90.def
dlltool --dllname msvcr90.dll --def msvcr90.def --output-lib libmsvcr90.a
