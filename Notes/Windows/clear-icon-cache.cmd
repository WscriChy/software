@echo off
set IconCache=%LocalAppData%\IconCache.db

If exist "%IconCache%" goto delID
echo.
echo Icon Cache has already been cleared.
echo.
pause
exit /B

:delID
echo Attempting to clear Icon Cache...
echo.
taskkill /IM explorer.exe /F
del "%IconCache%" /A
echo.
echo Icon Cache has been successfully cleared.
echo.
start explorer.exe
pause
exit /B
