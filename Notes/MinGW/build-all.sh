# semi-automated script to setup build environment with mingw64 using MSYS
# Installs the following packages:
#
#   0. gendef, ccache
#   1. python and a bunch of modules (numpy, matplotlib, PIL, bzr, ...)
#   2. boost (including boost::python)
#   3. Qt, PyQt (sip), QGLViewer
#   4. VTK, cmake
#   5. glut, GLE
#   6. GTS, pygts
#
# * Patches should be put in $TOPDIR/patches/*
# * Some utilities (ccache, bjam, gendef) are installed in $TOPDIR/utils
#     which might be added to the path


TOPDIR=/c/src
PREFIX=/c/MinGW64/
export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig
export CFLAGS="-I$PREFIX/include"
export LDFLAGS="-L$PREFIX/lib"
set -e -x


mkdir -p $TOPDIR
cd $TOPDIR

# mingw-get install msys-wget msys-patch msys-bzip2 msys-libbz2-dll msys-unzip

#
# prepare some utilities we will need
#
if false; then
	# gendef
	wget -c 'http://mingw-w64.svn.sourceforge.net/viewvc/mingw-w64/trunk/mingw-w64-tools/gendef/?view=tar' -O gendef.tar.gz
	tar xvfz gendef.tar.gz
	cd gendef; ./configure; make; cp gendef.exe $TOPDIR/utils; cd $TOPDIR
	# ccache
	wget 'http://ccache-win32.googlecode.com/issues/attachment?aid=30002002&name=ccache.exe&token=mAgV70iYl17wkGFBtX397xyQrvw%3A1352130806964' -O $TOPDIR/utils/ccache.exe
	ccache -M 500G -F 20G
fi

#
# prepare python
#
if false; then
	#wget -c http://www.python.org/ftp/python/2.7.3/python-2.7.3.amd64.msi
	## wait for the installation to finish here
	#echo 'Run the python-2.7.4.amd64.msi installer by hand, then type exit<ENTER>'
	cmd /C python-2.7.4.amd64.msi
	#
	cd /c/windows/system32; gendef.exe - python27.dll > /c/python27/libs/python27.def
	cd /c/python27/libs; dlltool.exe --dllname /c/windows/system32/python27.dll --def python27.def --output-lib libpython27.a
	#
	## diff -ur /c/python27.orig /c/python27 > /c/src/patches/python27.patch; dos2unix.exe /c/src/patch/espython27.patch
	cd $TOPDIR
	patch -d /c/python27 -p0 < $TOPDIR/patches/python27.patch
	#
	# force distutils to use our compiler
	#
	UNIXHOME=`python -c 'import os.path; print(os.path.expanduser("~"))'`
	echo -e '[build]\ncompiler=mingw32' > $UNIXHOME/pydistutils.cfg
	cp $UNIXHOME/pydistutils.cfg /c/Users/$USERNAME  # not sure if this is needed
	#
	# install easy_install
	#
	wget -c http://peak.telecommunity.com/dist/ez_setup.py
	python ez_setup.py

fi

cd $TOPDIR

if false; then
	cd $TOPDIR
    wget -c 'http://sourceforge.net/projects/boost/files/boost/1.51.0/boost_1_51_0.tar.bz2/download' -O $TOPDIR/boost_1_51_0.tar.bz2 
	tar xfj boost_1_51_0.tar.bz2
	# fix issue http://stackoverflow.com/questions/1704046/boostpython-windows-7-64-bit
	patch -p1 < $TOPDIR/patches/boost1.51.patch
	# zlib and bzip2
	wget -c http://zlib.net/zlib127.zip
	unzip zlib127.zip -d $TOPDIR
	wget -c http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz
	tar xvfz bzip2-1.0.6.tar.gz -C $TOPDIR
	cd boost_1_51_0
	echo 'Run "bootstrap.bat gcc" and "exit" in the shell'
	cmd
fi

if false; then
	## compile here

	cd boost_1_51_0
	
	./b2 -j4 toolset=gcc variant=release link=shared threading=multi \
		--with-python --with-filesystem --with-thread --with-regex \
		--with-random --with-serialization --with-date_time --with-iostreams --with-system \
		-sNO_ZLIB=0 -sZLIB_SOURCE=c:/src/zlib-1.2.7 \
		-sNO_BZIP2=0 -sBZIP2_SOURCE=c:/src/bzip2-1.0.6
				
	cp bjam.exe $TOPDIR/utils  # for compiling examples, for instance
fi

if false; then
	cd $TOPDIR
	wget --no-check-certificate -c http://bitbucket.org/eigen/eigen/get/3.1.1.zip -O eigen-3.1.1.zip
	unzip eigen-3.1.1.zip
	mv eigen-eigen-* eigen-3.1.1 # hash instead of version name
fi

if false; then
	#
	# install some python modules
	#
	easy_install xlwt xlrd genshi colorama pyreadline ipython
	easy_install minieigen
	easy_install bzr
fi


#
# configure bazaar
#
if true; then
	BZRPATH=`bzr version |grep 'Bazaar configuration' | cut -d: -f2,3`
	mkdir -p $BZRPATH/plugins
	bzr co --lightweight https://code.launchpad.net/~bzr/bzr-webdav/webdav $BZRPATH/plugins/webdav
	bzr co --lightweight https://code.launchpad.net/~qbzr-dev/qbzr/trunk2a $BZRPATH/plugins/qbzr
fi

##
## python modules which won't install via easy_install
##
mkdir $TOPDIR/bin-inst
cd $TOPDIR/bin-inst
wget -c http://www.lfd.uci.edu/~gohlke/pythonlibs/xmgmtjfu/numpy-unoptimized-1.6.2.win-amd64-py2.7.exe
wget -c http://www.lfd.uci.edu/~gohlke/pythonlibs/xmgmtjfu/matplotlib-1.2.0rc3.win-amd64-py2.7.exe
wget -c http://www.lfd.uci.edu/~gohlke/pythonlibs/xmgmtjfu/PIL-fork-1.1.7.win-amd64-py2.7.exe
## run those manually 


###
### those don't compile cleanly, get binaries from DEB packages
###

if false; then
	mkdir -p $TMPROOT
	for LIB in zlib_1.2.5 libxml2_2.7.7 libiconv_1.13.1 gettext_0.18; do
		DEB=mingw-w64-$LIB-2flosoft1_amd64.deb
		wget --no-check-certificate -c https://launchpad.net/~flosoft/+archive/cross-mingw/+files/$DEB
		#ar p $DEB data.tar.gz | tar xz -C /c/src/root ## PIPE BROKEN!
		ar xv $DEB data.tar.gz
		tar xvfz data.tar.gz -C $TMPROOT
		rm data.tar.gz
	done
	# relocate libs (hack)
	for f in $TMPROOT/usr/x86_64-pc-mingw32/lib/*.la; do
		sed -i 's@/usr/x86_64-pc-mingw32@/c/MinGW64@g' $f
	done

	cp -ar $TMPROOT/usr/x86_64-pc-mingw32/* $PREFIX
	rm -rf $TMPROOT

fi


if false; then
	cd $TOPDIR
	wget -c http://ftp.gnome.org/pub/gnome/sources/glib/2.26/glib-2.26.1.tar.bz2
	tar xfj glib-2.26.1.tar.bz2
	cd glib-2.26.1 && ./configure --prefix=$PREFIX && make -j8 && make install
fi

if false; then
	wget -c http://sourceforge.net/projects/gts/files/gts/0.7.6/gts-0.7.6.tar.gz/download -O gts-0.7.6.tar.gz
	tar xvfz gts-0.7.6.tar.gz
	cd gts-0.7.6 && ./configure --prefix=/c/MinGW64 && make && make install
fi

cd $TOPDIR
wget -c http://sourceforge.net/projects/pygts/files/pygts/0.3.1/pygts-0.3.1.tar.gz/download -O pygts-0.3.1.tar.gz
tar xvfz pygts-0.3.1.tar.gz
cd pygts-0.3.1
patch -p0 < $TOPDIR/patches/pygts-commands-module.patch
python setup.py install


if false; then
	wget -c http://www.cmake.org/files/v2.8/cmake-2.8.10.1.tar.gz
	tar xfz cmake-2.8.10.1.tar.gz
	cd cmake-2.8.10.1
	./configure --prefix=$PREFIX
	make && make install
fi

if true; then
	## build VTK
	wget -c http://www.vtk.org/files/release/5.10/vtk-5.10.1.tar.gz
	tar xfz vtk-5.10.1.tar.gz
	# fix for http://vtk.org/Bug/view.php?id=11742
	patch -p2 < $TOPDIR/patches/vtk5.10-1.patch
	rm -rf VTK5.10.1-build; mkdir -p VTK5.10.1-build
	cd VTK5.10.1-build
	cmake ../VTK5.10.1 -DVTK_USE_QT=off -DBUILD_EXAMPLES=off -DCMAKE_INSTALL_PREFIX:PATH=$PREFIX -DBUILD_SHARED_LIBS:BOOL=on -G"MSYS Makefiles"
	make -j1
	make install
fi

if false; then
	#wget -c http://releases.qt-project.org/qt4/source/qt-everywhere-opensource-src-4.8.3.tar.gz
	tar xfz qt-everywhere-opensource-src-4.8.3.tar.gz
	cd qt-everywhere-opensource-src-4.8.3
	./configure.exe -prefix c:\\MinGW64 -release -opensource -confirm-license -shared -fast \
		-no-webkit -no-multimedia -no-xmlpatterns -no-qt3support \
		-no-accessibility -no-script -no-declarative \
		-no-dsp -no-vcproj -no-s60 \
		-qt-zlib -qt-libtiff -qt-libpng -qt-libmng -qt-libjpeg \
		-nomake demos -nomake examples
	rm -rf $PREFIX/mkspecs
	cp -r mkspecs $PREFIX/mkspecs
	mingw32-make -j8
	make install
fi

if false; then
	cd $TOPDIR
	#wget -c http://sourceforge.net/projects/pyqt/files/sip/sip-4.14.1/sip-4.14.1.tar.gz
	#rm -rf sip-4.14.1
	tar xfz sip-4.14.1.tar.gz
	cd sip-4.14.1
	python configure.py -p win32-g++
	## this is ugly, but it is SIPs fault that is does not want to run in shell
	sed -i 's/@if not exist \(.*\) mkdir /mkdir -p /' Makefile */Makefile 2>/dev/null
	sed -i 's@copy /y@cp -r@' Makefile */Makefile 2>/dev/null
	mingw32-make
	make install
fi

if false; then
	cd $TOPDIR
	#wget -c http://sourceforge.net/projects/pyqt/files/PyQt4/PyQt-4.9.5/PyQt-win-gpl-4.9.5.zip
	unzip PyQt-win-gpl-4.9.5.zip
	cd PyQt-win-gpl-4.9.5
	python configure.py --confirm-license --no-designer-plugin --no-sip-files --concatenate -p win32-g++
	sed -i 's/@if not exist \(.*\) mkdir /mkdir -p /' Makefile */Makefile 2>/dev/null
	sed -i 's@copy /y@cp -r@' Makefile */Makefile 2>/dev/null
	mingw32-make -j8
	make install
fi

if false; then
	cd $TOPDIR
	#wget -c http://www.libqglviewer.com/src/libQGLViewer-2.3.17.zip
	rm -rf libQGLViewer-2.3.17
	unzip libQGLViewer-2.3.17.zip
	cd libQGLViewer-2.3.17/QGLViewer
	#qmake
	#for SUBDIR in QGLViewer examples examples/contribs; do
	#		( cd $SUBDIR && qmake )
	#done
	qmake
	sed -i 's@-TP -GR -Zi -EHs@@g' Makefile*
	sed -i 's@-fno-exceptions@-fexceptions@g' Makefile*
	mingw32-make -j8 release
	# make install # broken?!
	cp release/QGLViewer2.dll $PREFIX/lib
	mkdir -p $PREFIX/include/QGLViewer
	cp *.h $PREFIX/include/QGLViewer	
fi

if false; then
	cd $TOPDIR
	#wget -c http://sourceforge.net/projects/freeglut/files/freeglut/2.8.0/freeglut-2.8.0.tar.gz/download -O freeglut-2.8.0.tar.gz
	#tar xfz freeglut-2.8.0.tar.gz
	cd freeglut-2.8.0
	./configure --prefix=$PREFIX
	mingw32-make -j8
	mingw32-make install
fi

if false; then
	cd $TOPDIR
	#wget -c http://www.linas.org/gle/pub/gle-3.1.0.tar.gz
	#tar xvfz gle-3.1.0.tar.gz
	cd gle-3.1.0
	cp $TOPDIR/patches/gle-config.h config.h
	cd src
	#./configure --prefix=$PREFIX --without-x
	#mingw32-make
	#mingw32-make install
	#cd gle-3.1.0/src
	g++ -fpermissive -shared -O2 -DWIN32 $CFLAGS $LDFLAGS -I.. *.c -o gle.dll  -lglut -lopengl32 -lglu32 -lgdi32 
	cp gle.h $PREFIX/include/GL/
	cp gle.dll $PREFIX/lib
fi

