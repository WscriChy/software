When the program update-desktop-database is run (usually as root during the 
(un)installation of a package), it updates files called mimeinfo.cache in the 
/usr/local/share/applications and /usr/share/applications directories. These files keep 
track of which MIME-types are associated with which .desktop files overall. This file 
should not be edited by the user.

1. execute set_mime.sh
2. mv ~/.local/share/applications/mimeapps.list ~/.config
