

xdg-mime default firefox.desktop x-scheme-handler/http
xdg-mime default firefox.desktop x-scheme-handler/https

xdg-mime default nemo.desktop inode/directory

xdg-mime default gvim.desktop $(grep '^text/*' /usr/share/mime/types)
xdg-mime default gvim.desktop application/x-shellscript
xdg-mime default kde4-okularApplication_pdf.desktop application/pdf

cat ~/.local/share/applications/mimeapps.list > ~/.config/mimeapps.list
