#!/usr/bin/env bash

nohup /home/wscrichy/.conky/conky-startup.sh > /dev/null &

cd ~

nohup guake > /dev/null &

nohup tint2 > /dev/null &
