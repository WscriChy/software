#!/usr/bin/env python
# encoding: utf-8

import os
import re
import subprocess

#To use the multilib repository, uncomment the [multilib] section in /etc/pacman.conf (Please be sure to uncomment both two lines):
#[multilib]
#Include = /etc/pacman.d/mirrorlis

packamanIsPackageInstalledCommand = "pacman -Qi"
packamanIsGroupInstalledCommand = "pacman -Qg"
packamanInstallCommand = "pacman -Sy --noconfirm"
aurgetInstallCommand = "aurget -Sy --asroot --noconfirm"

packages = [
  #1) autoconf  2) automake  3) binutils  4) bison  5) fakeroot
  #6) file  7) findutils  8) flex  9) gawk  10) gcc  11) gettext
  #12) grep  13) groff  14) gzip  15) libtool  16) m4  17) make
  #18) pacman 19) patch  20) pkg-config  21) sed  22) sudo  23) texinfo
  #24) util-linux  25) which
  "base-devel",
  "tar",
  "curl",
  "wget",
  "gdb",
  "cmake",
  "scons",
  "clang",
  "ninja",
  "ack",
  "zsh",
  "doxygen",
  "openssh",
  "git",
  "libgit2",
  "mercurial",
  "subversion",

  "ranger",
  "atool",
  "highlight",
  "libcaca",
  "mediainfo",
  "perl-image-exiftool",
  "poppler",
  "python-chardet",
  "transmission-cli",
  "w3m",

  "p7zip",
  "zip",
  "unzip",

  "lsof",

  "jre8-openjdk",
  "jdk8-openjdk",
  "apache-ant",
  "maven",

  "mono",

  "eigen",
  "boost",
  "intel-tbb",
  "glew",
  "libcl",
  "sdl2",
  "icu",
  "openmpi",
  "qt5",

  "mariadb", # mysql replacement (does not have mysql)
  "sqlite",

  "gvim", # Already has vim (conflict with vim)
  "ctags",

  "ntp",
  "wireless_tools",

  "texlive-most",
  "texlive-lang",

  "conky",
  "cairo-dock",
  "cairo-dock-plugins",
  "faenza-icon-theme",

  "galculator",
  "evince", # xpdf is in Aur
  "chromium",
  "firefox",
  "thunderbird",
  "vlc",
  "nomacs",
  "skype",
  "gimp",
  "inkscape",
  "libreoffice-still",
  "libreoffice-still-en-US",
  "libreoffice-still-ru",
  "libreoffice-still-de",
  "libreoffice-still-gnome",
  "gnome-system-monitor",
  "guake",
  #"gnome-terminal",

  "flashplayer",

  "python2-numpy",
  "python2-matplotlib",

  #Monitorix stuff
  #"hddtemp", #enable support for hdd temp monitoring.
  #"smartmontools", #enable support for hdd bad sector monitoring.
  #"terminus-font", #if graphs do not contain characters, you may need this font package.
  ]

aurPackages = [
  "ttf-ms-fonts",

  "delorean-dark",
  #"square-icon-theme",

  "libgee06",
  "conky-manager",
  "compiz",

  "pandoc-static",

  "canon-pixma-mx390-complete",
  "chromium-pepper-flash",

  "libcgns-paraview",
  #"paraview",
  ]


def makeCommand(*args):
  result = []
  for arg in args:
    if isinstance(arg, str): # Python 3: isinstance(arg, str)
        result.extend(arg.split(" "))
    else:
        result.extend(arg)
  return " ".join(result)

def isPackageInstalled(packageName):
  process = subprocess.Popen(makeCommand(packamanIsPackageInstalledCommand,
                                         packageName),
      stdin  = subprocess.PIPE,
      stdout = subprocess.PIPE,
      stderr = subprocess.PIPE,
      shell  = True)
  out, err = process.communicate()
  result1 = out.decode("utf-8").strip() == ""
  process = subprocess.Popen(makeCommand(packamanIsGroupInstalledCommand,
                                         packageName),
      stdin  = subprocess.PIPE,
      stdout = subprocess.PIPE,
      stderr = subprocess.PIPE,
      shell  = True)
  out, err = process.communicate()
  result2 = out.decode("utf-8").strip() == ""
  if result1 and result2:
    return False
  return True

def installAurget():
  if isPackageInstalled("aurget"):
    return
  aurgetURL = "https://aur.archlinux.org/packages/au/aurget/aurget.tar.gz"
  aurgetPackagePath = "/home/wscrichy/Downloads/aurget",

  subprocess.call("", cwd = "/home/wscrichy/Downloads", shell = True)
  subprocess.call("curl -L -O {0}".format(aurgetURL),
          cwd = "/home/wscrichy/Downloads",
          shell = True)

  subprocess.call("tar -xvf {0}".format(os.path.basename(aurgetURL)),
          cwd = "/home/wscrichy/Downloads",
          shell = True)

  subprocess.call("makepkg -s --asroot {0}".format(os.path.basename(aurgetURL)),
          cwd = aurgetPackagePath,
          shell = True)

  for f in os.listdir(aurgetPackagePath):
    if os.path.isfile(os.path.join(aurgetPackagePath,f)):
      match = re.match("aurget.*\\.pkg\\.tar\\.xz", f);
      if match != None:
        subprocess.call("pacman -U {0}".format(f),
                cwd = aurgetPackagePath,
                shell = True)
        break

def installPackages(packages):
  for package in packages:
    if not isPackageInstalled(package):
      subprocess.call(makeCommand(packamanInstallCommand, package), shell = True)

def installAurPackages(packages):
  for package in packages:
    if not isPackageInstalled(package):
      subprocess.call(makeCommand(aurgetInstallCommand, package), shell = True)

installPackages(packages)
installAurget()
installAurPackages(aurPackages)
