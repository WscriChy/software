#!/usr/bin/env python
# encoding: utf-8

import os
import re
import subprocess

#sudo add-apt-repository ppa:noobslab/themes
#sudo apt-get update
#sudo apt-get install delorean-dark-3.12
#sudo apt-get install square-icons
#sudo apt-get install sable-gtk
#sudo apt-get install ambiance-dark-red

packamanIsPackageInstalledCommand = "dpkg -s"
packamanInstallCommand = "apt-get install -y"

packages = [
  "silversearcher-ag",
  "libevdev-dev",
  "tar",
  "curl",
  "wget",
  "gdb",
  "cmake",
  "scons",
  "clang",
  "libclang-dev",
  "clang-format-3.6",
  "ninja-build",
  "ack-grep",
  "zsh",
  "doxygen",
  # "openssh",
  "git",
  "libgit2-dev",
  "mercurial",
  "subversion",
  "exuberant-ctags",
  "uncrustify",
  "vim-gtk",
  "tmux",
  "xclip",
  "coreutils", #tee

  "openntpd",

  "ranger",
  "atool",
  "highlight",
  "caca-utils",
  "poppler-utils",
  "python-chardet",
  "elinks",
  "w3m-img",

  "rar",
  "unrar",
  "p7zip-full",
  "zip",
  "unzip",

  "lsof",
  "ncdu",

  "default-jdk",
  "default-jre",
  "ant",
  "maven",

  # "mono",

  # "libeigen3-dev",
  # "libboost-all-dev",
  # "libtbb-dev",
  # "libglew-dev",
  # "open cl",
  # "sdl2",
  # "libicu-dev",
  # "libopenmpi-dev",
  # "libpetsc3.4.2-dbg",
  # "libpetsc3.4.2-dev",
  # "qt5-default",

  "python-all-dev",
  # "python-numpy",
  "python-pip",

  # "fonts-arphic-uming",
  # "fonts-arphic-ukai",
  "ttf-ancient-fonts",

  "texlive-latex-base",
  "texlive-latex-extra",
  "texlive-math-extra",
  "texlive-latex-recommended",
  "texlive-latex-base",
  "texlive-xetex",
  "texlive-bibtex-extra",
  "biber",

  "chromium-browser",
  "kdebase-bin",
  "kdesudo",
  "dolphin",
  "okular",
  "firefox",
  "tor",
  "wicd-gtk",

  "pavucontrol",
  "systemsettings",
  "xfce4-mixer",
  "xfce4-settings",
  "xfce4-session",
  "xfce4-appfinder",
  "xfce4-power-manager",
  "xfconf",
  "xfdesktop4",
  "xfwm4",

  "guake",
  "gnome-terminal",
  "gnome-system-monitor",

  "tint2",
  # "conky",

  "inkscape",
  "gimp",
  "blender",
  "deluge",
  "vlc"
  ]


def makeCommand(*args):
  result = []
  for arg in args:
    if isinstance(arg, str): # Python 3: isinstance(arg, str)
        result.extend(arg.split(" "))
    else:
        result.extend(arg)
  return " ".join(result)

def isPackageInstalled(packageName):
  process = subprocess.Popen(makeCommand(packamanIsPackageInstalledCommand,
                                         packageName),
      stdin  = subprocess.PIPE,
      stdout = subprocess.PIPE,
      stderr = subprocess.PIPE,
      shell  = True)
  out, err = process.communicate()
  result1 = out.decode("utf-8").strip() == ""
  if result1:
    return False
  return True

def installPackages(packages):
  for package in packages:
    if not isPackageInstalled(package):
      subprocess.call(makeCommand(packamanInstallCommand, package), shell = True)

installPackages(packages)
