# Configure Boost54 {{{
# -----------------------------------------------------------------------------
#
# set(Boost_Libs log thread system)
# Boost_Libs(Boost_Libs)

macro(Boost Boost_Libs)
  set(BOOST_INCLUDE_DIR ${Boost_INCLUDE_DIR})
  set(BOOST_LIBRARY_DIR ${Boost_LIBRARY_DIR})

  if ("${BOOST_INCLUDE_DIR}" STREQUAL "")
    set(BOOST_INCLUDE_DIR $ENV{Boost_INCLUDE_DIR})
  endif ()

  if ("${BOOST_LIBRARY_DIR}" STREQUAL "")
    set(BOOST_LIBRARY_DIR $ENV{Boost_LIBRARY_DIR})
  endif ()

  if (NOT DEFINED Boost_USE_STATIC_LIBS)
    set(Boost_USE_STATIC_LIBS ON)
  endif()
  if (NOT DEFINED Boost_USE_MULTITHREADED)
    set(Boost_USE_MULTITHREADED ON)
  endif()
  if (NOT DEFINED Boost_USE_STATIC_RUNTIME)
    set(Boost_USE_STATIC_RUNTIME ON)
  endif()

  find_package(Boost
    1.54.0
    COMPONENTS ${Boost_Libs} REQUIRED)

  include_directories(${Boost_INCLUDE_DIRS})
endmacro()
# -----------------------------------------------------------------------------
# Configure Boos54 }}}
