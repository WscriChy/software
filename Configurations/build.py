#!/usr/bin/env python
# encoding: utf-8

import os, sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'Distributions', 'qt'))

from Object import Object

from Builder import Builder

mainConfig = Object(
    cmake = Object(
      symlink = {
        "Boost.cmake" : [
          "E:/Documents/C++Projects/PluginManager/cmake/modules"
          ]
        }
      ) # cmake
    )


rootPath = os.path.dirname(__file__)

publicAttrs = (entity for entity in dir(mainConfig)
    if not entity.startswith('_') and not entity.startswith('__'))
for entity in publicAttrs:
  target = mainConfig[entity]
  targetPath = os.path.join(rootPath, entity)
  if hasattr(target, "symlink"):
    for key, value in target["symlink"]:
      symlinkEntityPath = os.path.join(targetPath, key)
      symlinkTargetPath = os.path.join(value, key)
      if not os.path.exists(symlinkTargetPath):

      else:
        print("Warning: Path exists:\n%s" % symlinkTargetPath)



