#!/bin/bash.exe

unset MAILCHECK
ulimit -c 0

#HOME="/home/mobaxterm"
NP=0
#PROMPT_COMMAND='r0=$?;if [ -z "$NP" ]; then i0=0;s0="";while [ "$i0" -lt "${COLUMNS:-80}" ];do s0="q$s0";i0=$[$i0+1];done;builtin echo -ne "\n\E[1;30m\E(0$s0\E(B\E[0m"; [ $r0 == 0 ] && builtin echo -ne "\e[1A\e[32m\e(0d\e(B\e[0m\e[1B" || builtin echo -ne "\e[1A\e[31m\e(0e\e(B\e[0m\e[1B";else unset NP;fi;'
#PS1='\[\033]0;$PWD\007\]
#\[\033[33m\][\D{%Y-%m-%d %H:%M.%S}]\[\033[0m\]  \[\033[35m\]\w\[\033[0m\]
#\[\033[36m\][\u.\h]\[\033[0m\] \[\033(0\]b\[\033(B\] '

TMOUT=0

shopt -s nocaseglob
shopt -s dotglob

## Colorize the ls output ##
alias ls='ls --color=auto'

## Use a long listing format ##
alias ll='ls -la'

## Show hidden files ##
alias l.='ls -d .* --color=auto'

alias l='ls'
alias r='ranger'

function cd() {
new_directory="$*";
if [ $# -eq 0 ]; then
ls
else
builtin cd "${new_directory}" && ls
fi;
}

#alias cd=$cdls

## get rid of command not found ##
alias cd..='cd ..'

## a quick way to get out of current directory ##
alias ..='cd ..'
alias ...='cd ../../../'
alias ....='cd ../../../../'
alias .....='cd ../../../../'
alias .4='cd ../../../../'
alias .5='cd ../../../../..'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

#mkdir command is used to create a directory:
alias mkdir='mkdir -pv'

alias path='echo -e ${PATH//:/\\n}'
alias now='date +"%T'
alias nowtime=now
alias nowdate='date +"%d-%m-%Y'

alias dwn='cd ~/Downloads'
alias doc='cd /e/Documents'
alias cpp='cd /e/Documents/C++Projects'
alias lbm='cd /e/Documents/C++Projects/LBM'

alias gc="git add -A && git commit -m"

txtblk='\[\e[0;30m\]' # Black - Regular
txtred='\e[0;31m' # Red
txtgrn='\[\e[0;32m\]' # Green
txtylw='\[\e[0;33m\]' # Yellow
txtblu='\[\e[0;34m\]' # Blue
txtpur='\[\e[0;35m\]' # Purple
txtcyn='\[\e[0;36m\]' # Cyan
txtwht='\[\e[0;37m\]' # White

bldblk='\[\e[1;30m\]' # Black - Bold
bldred='\[\e[1;31m\]' # Red
bldgrn='\[\e[1;32m\]' # Green
bldylw='\[\e[1;33m\]' # Yellow
bldblu='\[\e[1;34m\]' # Blue
bldpur='\[\e[1;35m\]' # Purple
bldcyn='\[\e[1;36m' # Cyan
bldwht='\[\e[1;37m' # White

unkblk='\[\e[4;30m\]' # Black - Underline
undred='\[\e[4;31m\]' # Red
undgrn='\[\e[4;32m\]' # Green
undylw='\[\e[4;33m\]' # Yellow
undblu='\[\e[4;34m\]' # Blue
undpur='\[\e[4;35m\]' # Purple
undcyn='\[\e[4;36m\]' # Cyan
undwht='\[\e[4;37m\]' # White

bakblk='\[\e[40m\]'   # Black - Background
bakred='\[\e[41m\]'   # Red
badgrn='\[\e[42m\]'   # Green
bakylw='\[\e[43m\]'   # Yellow
bakblu='\[\e[44m\]'   # Blue
bakpur='\[\e[45m\]'   # Purple
bakcyn='\[\e[46m\]'   # Cyan
bakwht='\[\e[47m\]'   # White

txtrst='\e[0m'    # Text Reset

#export CLICOLOR=1
# export LS_COLORS="di=$txtblu:fi=1:ln=31:pi=5:so=5:bd=5:cd=5:or=31:*.exe=$txtred:*.deb=90"
export LS_COLORS="di=34:fi=0:*.exe=31"
export HISTFILESIZE=1000000
export HISTSIZE=1000000
# Ignore duplicate bash shell commands
export HISTCONTROL=ignoreboth
# Ignore multiple specific bash shell commands
export HISTIGNORE="ignoreboth:ls:pwd:exit:mount" # (history will ignore the ls, pwd, exit, and mount commands)

#PS1="\\[$(tput setaf 1)\\]\\u@\\h:\\w #\\[$(tput sgr0)\\]"

PrintBeforeThePrompt () {
  printf "$txtred%s \n$txtrst" "$PWD"
}

PROMPT_COMMAND=PrintBeforeThePrompt

PS1="$bldpur>>> \[\e[0m\]"

shopt -s checkwinsize

extract () {
   if [ -f $1 ] ; then
       case $1 in
           *.tar.bz2)   tar xvjf $1    ;;
           *.tar.gz)    tar xvzf $1    ;;
           *.bz2)       bunzip2 $1     ;;
           *.rar)       unrar x $1       ;;
           *.gz)        gunzip $1      ;;
           *.tar)       tar xvf $1     ;;
           *.tbz2)      tar xvjf $1    ;;
           *.tgz)       tar xvzf $1    ;;
           *.zip)       unzip $1       ;;
           *.Z)         uncompress $1  ;;
           *.7z)        7z x $1        ;;
           *)           echo "don't know how to extract '$1'..." ;;
       esac
   else
       echo "'$1' is not a valid file!"
   fi
 }

# smart advanced completion, download from
# http://bash-completion.alioth.debian.org/
# if [[ -f $HOME/bash_completion ]]; then
#     . $HOME/bash_completion/bash_completion
# fi

PATH=$PATH

# vi mode
set -o vi

# Append history instead of rewriting it
# You should start by setting the histappend option, which will mean that when you close a session, your history will be appended to the .bash_history file rather than overwriting what’s in there.
shopt -s histappend

# Use one command per line
# To make your .bash_history file a little easier to parse, you can force commands that you entered on more than one line to be adjusted to fit on only one with the cmdhist option:
shopt -s cmdhist

export HOME PS1 PROMPT_COMMAND NP
export HISTCONTROL HISTFILE HISTSIZE TMOUT
