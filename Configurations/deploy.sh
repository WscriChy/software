#!/usr/bin/env bash
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cp -r -u -p $DIR/Zsh/zsh ~/.config/
ln -s $DIR/Tmux/.tmux.conf ~/
