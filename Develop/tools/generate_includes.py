#!/usr/bin/env python
# encoding: utf-8

import inspect
import os
import sys

current_dir = os.path.dirname(os.path.realpath(
    os.path.abspath(inspect.getfile(inspect.currentframe()))))

project_dir = os.path.dirname(current_dir)
python_dir = os.path.join(current_dir, 'python')

if python_dir not in sys.path:
    sys.path.insert(0, python_dir)

from include_generator import generate_includes

source = os.path.join(project_dir, "source")
destination = os.path.join(project_dir, "include", "DependencyManager")
include_files = {
    "Facade/ApplicationContext" : "ApplicationContext",
    "Facade/ApplicationContextLoader" : "ApplicationContextLoader",
    "Facade/ObjectFactory" : "ObjectFactory",
    "Facade/XmlApplicationContextLoader" : "XmlApplicationContextLoader",
    "MetaSystem/MetaTypeRegistration" : "",
    "MetaSystem/MetaObject" : "",
    "MetaSystem/QtMetaStorage" : "",
    "Core/Loader" : "Loader",
    "Core/SharedPtr" : "SharedPtr",
    "Core/Visibility" : "Visibility"
    }

def main(deepcopy = False):
  generate_includes(source, destination, include_files, deepcopy)

if __name__ == "__main__":
    main()


# vim:ft=python:fenc=utf-8:ff=unix:ts=2:sw=2:tw=80:et:
