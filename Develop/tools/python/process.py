import subprocess
import sys


def call(command, cwd):
    if sys.platform.startswith('win'):
        shell = True
    else:
        shell = False
    subprocess.call(command, cwd=cwd, shell=shell)
