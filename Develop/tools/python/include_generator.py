#!/usr/bin/env python
# encoding: utf-8

import os
from os.path import join, normpath, exists, relpath
import shutil
import codecs

def _get_shallow_header_content(originalFile):
    return ((
            "#include \"%s\"\n"
            "// vim:ft=cpp:fenc=utf-8:ff=unix:ts=2:sw=2:tw=80:et:") %(
              originalFile))

def generate_includes(source,
                      destination,
                      include_files,
                      deepcopy = False):
  destinationPath = destination #.join(destination, prefix)
  if not exists(destinationPath):
    os.makedirs(destinationPath)
  for file, output in include_files.iteritems():
    originalFilePath = normpath(join(source, file))
    if not exists(originalFilePath):
      raise Exception("File '%s' does not exist" % originalFilePath)
    newFilePath = file
    if output:
      newFilePath = output
    newFilePath = normpath(join(destinationPath, newFilePath))
    newFileDirPath = os.path.dirname(newFilePath)
    if not exists(newFileDirPath):
      os.makedirs(newFileDirPath)
    if deepcopy:
      shutil.copy(newFilePath, originalFilePath)
    else:
      fileHandler = codecs.open(newFilePath, 'w+', "utf-8")
      fileHandler.write(_get_shallow_header_content(
        relpath(originalFilePath, newFileDirPath)))
      fileHandler.close()


# vim:ft=python:fenc=utf-8:ff=unix:ts=2:sw=2:tw=80:et:
