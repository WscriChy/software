if (CMAKE_VERSION VERSION_LESS 2.8.3)
  message(FATAL_ERROR "DependencyManager requires at least CMake version 2.8.3")
endif()

set(DependencyManager_FOUND False)

get_filename_component(_DependencyManager_INSTALL_PREFIX "${CMAKE_CURRENT_LIST_DIR}/../" ABSOLUTE)

set(DependencyManager_VERSION_STRING 0.0.1)

macro(_dependency_manager_check_file_exists file Target)
  if(NOT EXISTS "${file}" )
    message(FATAL_ERROR "The imported target \"${Target}\" references the file
    \"${file}\"
    but this file does not exist.  Possible reasons include:
    * The file was deleted, renamed, or moved to another location.
    * An install or uninstall procedure did not complete successfully.
    * The installation package was faulty and contained
    \"${CMAKE_CURRENT_LIST_FILE}\"
    but not all the files it references.")
  endif()
endmacro()

macro(_populate_dependency_manager_target_properties Target Configuration Dependencies LIB_LOCATION IMPLIB_LOCATION)
  set_property(TARGET ${Target} APPEND PROPERTY IMPORTED_CONFIGURATIONS ${Configuration})
  set_target_properties(${Target} PROPERTIES
    "IMPORTED_LINK_INTERFACE_LIBRARIES_${Configuration}" "${Dependencies}"
    "IMPORTED_LOCATION_${Configuration}" ${LIB_LOCATION}
    )
  if(NOT "${IMPLIB_LOCATION}" STREQUAL "")
      set_target_properties(${Target} PROPERTIES
      "IMPORTED_IMPLIB_${Configuration}" ${IMPLIB_LOCATION}
      )
  endif()
endmacro()

if (NOT TARGET DependencyManager)
  find_package(Uni REQUIRED)

  add_library(DependencyManager SHARED IMPORTED)

  set(DependencyManager_LIBRARY_DIR "${_DependencyManager_INSTALL_PREFIX}/lib")
  set(DependencyManager_BINARY_DIR "${_DependencyManager_INSTALL_PREFIX}/bin")

  include(MacroPushRequiredVars)

  if(UNIX)
    macro_push_variables(CMAKE_FIND_LIBRARY_SUFFIXES)
    set(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
    find_library(DependencyManager_IMPRT_LIBRARY
               NAMES DependencyManager
               PATHS ${DependencyManager_LIBRARY_DIR})
    macro_pop_variables(CMAKE_FIND_LIBRARY_SUFFIXES)
    find_library(DependencyManager_LIBRARY
             NAMES DependencyManager
             PATHS ${DependencyManager_LIBRARY_DIR})
  else()
    find_library(DependencyManager_IMPRT_LIBRARY
               NAMES DependencyManager
               PATHS ${DependencyManager_LIBRARY_DIR})
    find_library(DependencyManager_LIBRARY
             NAMES DependencyManager
             PATHS ${DependencyManager_BINARY_DIR})
  endif()

  _populate_dependency_manager_target_properties(DependencyManager RELEASE "" ${DependencyManager_LIBRARY} ${DependencyManager_IMPRT_LIBRARY})
  _populate_dependency_manager_target_properties(DependencyManager DEBUG "" ${DependencyManager_LIBRARY} ${DependencyManager_IMPRT_LIBRARY})

  list(APPEND DependencyManager_INCLUDE_DIRS "${_DependencyManager_INSTALL_PREFIX}/include")
  list(APPEND DependencyManager_INCLUDE_DIRS "${Loki_INCLUDE_DIRS}")

  _dependency_manager_check_file_exists(${DependencyManager_INCLUDE_DIRS} DependencyManager)

  set_property(TARGET DependencyManager PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${DependencyManager_INCLUDE_DIRS})

  set(DependencyManager_DEFENITIONS "-DDependencyManager_SHARED ${Loki_DEFENITIONS}")

  set_property(TARGET DependencyManager PROPERTY INTERFACE_COMPILE_DEFINITIONS ${DependencyManager_DEFENITIONS})

  set(DependencyManager_FOUND True)

  message(STATUS "Found DependencyManager Library: ${DependencyManager_IMPRT_LIBRARY} ${DependencyManager_LIBRARY}")
endif()
